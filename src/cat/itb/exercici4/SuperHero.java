package cat.itb.exercici4;

import java.util.Random;

public class SuperHero {
    public final static String[] noms = {"Superman", "Spiderman", "CatWoman", "Wolverine", "SuperLopez"};
    private String nom;
    private int power;


    public SuperHero() {
        nom = noms[new Random().nextInt(noms.length)];
        power = new Random().nextInt(10) + 1;
    }

    @Override
    public String toString() {
        return "SuperHero{" +
                "nom='" + nom + '\'' +
                ", power=" + power +
                '}';
    }
}
