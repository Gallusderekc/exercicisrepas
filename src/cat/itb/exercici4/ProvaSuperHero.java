package cat.itb.exercici4;

import java.util.concurrent.*;

public class ProvaSuperHero {
    public static void main(String[] args) {

        Callable<SuperHero> c1 = () -> new SuperHero();
        Callable<SuperHero> c2 = () -> new SuperHero();
        Callable<SuperHero> c3 = () -> new SuperHero();

        ExecutorService es = Executors.newFixedThreadPool(3);

        Future<SuperHero> f1 = es.submit(c1);
        Future<SuperHero> f2 = es.submit(c2);
        Future<SuperHero> f3 = es.submit(c3);

        try {
            System.out.println(f1.get());
            System.out.println(f2.get());
            System.out.println(f3.get());
            es.shutdown();
        }catch(InterruptedException | ExecutionException e){}

    }


}
