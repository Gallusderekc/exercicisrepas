package cat.itb.exercici5;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;

public class TestLlistaPatinets {
    public static void main(String[] args) {
        ArrayList<Patinet> llista=new ArrayList();
        LocalTime inici = LocalTime.now();
        final int DISTANCIA = 1000;
        Patinet p1 = new Patinet("Montse", DISTANCIA, inici);
        Patinet p2 = new Patinet("Fran", DISTANCIA, inici);
        Patinet p3 = new Patinet("Clara", DISTANCIA, inici);

        llista.add(p1);
        llista.add(p2);
        llista.add(p3);

        for(Patinet p: llista){
            p.start();
        }

        for(Patinet p: llista){
            try {
                p.join();
            }catch (InterruptedException e){}
        }

     Collections.sort(llista);
        for(Patinet p: llista){
            System.out.println(p);
        }

        System.out.println("El patinet més ràpid és el de "+llista.get(0).getNom()+" i ha trigat "+llista.get(0).getTemps());
    }
}
