package cat.itb.exercici5;

import java.time.Duration;
import java.time.LocalTime;

/**Exercici 1 de l'examen parcial M9.UF2
 * @version 19/11/2020
 * @author Montse
 *
 */
public class Patinet extends Thread implements Comparable<Patinet> {
    private String nom; //nom del propietari del patinet
    private LocalTime inici; //moment en què comença el viatge en patinet
    private int distancia; // distància a recórrer
    private long temps; //temps transcorregut entre inici i completar la distància


    public Patinet(String n, int d, LocalTime ini) {
        inici = ini;
        nom = n;
        distancia = d;
    }

    public String getNom() {
        return nom;
    }

    public long getTemps() {
        return temps;
    }

    @Override
    public void run() {

        for (int i = 0; i < distancia; i++) {
        }
    //quan acaba el bucle és quan sabem que ha acabat el recorregut el patinet
        LocalTime acabat = LocalTime.now();
       temps = Duration.between(inici, acabat).getNano();
       // temps=(int)(Math.random()*1000);
//            try{
//                Thread.sleep(1000);
//            }catch(InterruptedException e){}
    }

    @Override
    public String toString() {
        String aux = nom + " ha trigat " + temps + " unitats de temps en fer una distancia de " + distancia;
        return aux;
    }

    /* aquest compareTo és necessari si volem tenir els patinets en una llista i ordenar la llista per temps*/
    @Override
    public int compareTo(Patinet p) {
        return Long.compare(temps, p.getTemps());
    }
}
