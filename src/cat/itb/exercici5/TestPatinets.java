package cat.itb.exercici5;

import java.time.LocalTime;

/** Prova de l'exercici dels patinets. 3 patinets separats.
 * @author Montse
 * @version 19/11/2020
 */
public class TestPatinets {
    public static void main(String[] args) {
        LocalTime inici = LocalTime.now();
        final int DISTANCIA = 1000;
        Patinet p1 = new Patinet("Montse", DISTANCIA, inici);
        Patinet p2 = new Patinet("Fran", DISTANCIA, inici);
        Patinet p3 = new Patinet("Clara", DISTANCIA, inici);

        p1.start();
        p2.start();
        p3.start();

        try {
            p1.join();
            p2.join();
            p3.join();
        } catch (InterruptedException e) {
        }

        System.out.println(p1);
        System.out.println(p2);
        System.out.println(p3);


        Patinet rapid;
        if (p2.getTemps() < p1.getTemps()) rapid = p2;
        else rapid = p1;
        if (p3.getTemps() < rapid.getTemps()) rapid = p3;

        System.out.println();
        System.out.println("El patinet més ràpid és el de " + rapid.getNom());
    }
}
