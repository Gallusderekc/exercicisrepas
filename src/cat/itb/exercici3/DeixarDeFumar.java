package cat.itb.exercici3;

public class DeixarDeFumar {

    private static Thread t1;
    private static Thread t2;

    public static void main(String[] args) {
        Runnable r1 = () -> fumador();
        t1 = new Thread(r1);
        t1.start();
        Runnable r2 = () -> metge();
        t2 = new Thread(r2);
        t2.start();


    }

    private static void metge() {
        try {
            Thread.sleep(3000);
            t1.interrupt();
            System.out.println("Deixar de fumar...");
        } catch (InterruptedException e) {
        }


    }

    private static void fumador()  {
        int i = 0;
        while (!t1.isInterrupted()) {
            System.out.println("Fumo una cigarreta..." + i);
            i++;
        }
        System.out.println("vale ya paro! ");
    }

}



