package cat.itb.exercici2;

public class TestNevera {
    public static void main(String[] args) {
        Nevera nevera=new Nevera(4);

        Thread t1=new Thread(()->nevera.afegeixCervesa(), "Pepi");
        Thread t2=new Thread(()->nevera.beuCervesa(),"Luci");
        Thread t3=new Thread(()->nevera.beuCervesa(),"Bom");
        Thread t4=new Thread(()->nevera.beuCervesa(),"Anna");

        t1.start();
        t2.start();
        t3.start();
        t4.start();

    }


}
