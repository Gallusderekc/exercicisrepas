package cat.itb.exercici1;

import java.util.Collection;
import java.util.Collections;

public class Main {
    public static void main(String[] args) {
        Thread t1=new Thread(new PrintThread("Una vegada hi havia un gat"));
        Thread t2=new Thread(new PrintThread("Once upon a time in the west"));
        Thread t3=new Thread(new PrintThread("En un lugar de la Mancha"));
        t1.start();
        t2.start();
        t3.start();
    }
}

class PrintThread implements Runnable {
    private String s1;
    public PrintThread(String a) {
        s1 = a;
    }

    @Override
    public void run() {
        mostrar(s1);
    }

    public static synchronized void mostrar(String s1){
        String []paraules=s1.split(" "); //separa per espais en blanc
        for (String p : paraules) {
            System.out.print(p+" ");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
            }
        }
        System.out.println();
    }
}
